//!
//! 
//! 
//! References (code)
//!     - python example : https://www.stefaanlippens.net/oauth-code-flow-pkce.html 
//! 
//! References :
//!     - https://assets.digitalocean.com/articles/oauth/auth_code_flow.png 
//!     - https://www.researchgate.net/profile/Amir-Sharif/publication/334989508/figure/fig2/AS:812820195205120@1570802844053/Authorization-Code-Flow-with-PKCE.png 
//!     - https://koenig-media.raywenderlich.com/uploads/2022/04/OAUTH-3.png
//!     - https://koenig-media.raywenderlich.com/uploads/2022/04/OAUTH_PKCE-3.png
//!     - https://yonghyunlee.gitlab.io/assets/img/oauth-authorization-code-1.png
//!     - https://124749134-files.gitbook.io/~/files/v0/b/gitbook-legacy-files/o/assets%2F-LHqtKiuedlcKJLm337_%2F-LYXMGgaEeuDes3fF72K%2F-LYXU4quGW1eiHsVd8cB%2FUntitled%20Diagram-Page-3.svg?alt=media&token=152dc8bf-e9d8-4600-91a7-4dd906207045
//!     - https://www.researchgate.net/profile/Atallah-Khedrane/publication/335686500/figure/fig5/AS:800856169537536@1567950398537/Authorization-code-Flow-in-OAuth-20.ppm
//!     - https://i0.wp.com/christianlydemann.com/wp-content/uploads/2018/05/oidc-code-flow-pkce-1.png?w=791&ssl=1
//!     - https://q4p5x4j6.rocketcdn.me/wp-content/uploads/2023/05/blog-aduneo.jpg.webp
//!     - https://blog.postman.com/wp-content/uploads/2023/10/Authorization-code-grant-flow.png
//!     - https://infosec.mozilla.org/guidelines/assets/images/OIDC_sequence_diagram.png 
//!

#![allow(unused)]

#![allow(clippy::expect_fun_call)]
extern crate env_logger;
extern crate http;
#[macro_use]
//extern crate log;
//extern crate openidconnect;
#[macro_use]
//extern crate pretty_assertions;
//extern crate reqwest_ as reqwest;
//extern crate url;

use std::collections::HashMap;
use std::process::exit;

use http::header::{LOCATION, SET_COOKIE};
use http::method::Method;
use http::{HeaderMap, HeaderValue, header::{COOKIE}};
//use reqwest::{blocking::Client, redirect::Policy, Url};
use openidconnect::reqwest;
//use url::Url;

use regex::Regex;

use std::io::BufReader;
use std::io::BufRead;
use anyhow::anyhow;
use std::net::TcpListener;

use std::sync::Arc; //cookies
//FSI: use reqwest_::cookie::Jar;

use openidconnect::core::{
    CoreClient, 
    CoreAuthenticationFlow,
    CoreClientAuthMethod, 
    CoreClientRegistrationRequest,
    CoreClientRegistrationResponse, 
    CoreIdToken, 
    CoreIdTokenClaims, 
    CoreIdTokenVerifier,
    CoreJsonWebKeySet, 
    CoreJwsSigningAlgorithm, 
    CoreProviderMetadata, 
    CoreResponseType,
    CoreUserInfoClaims,
};
use openidconnect::Nonce;
use openidconnect::{
    IssuerUrl,
    ClientId,
    ClientSecret,
    RedirectUrl,
    PkceCodeChallenge,
    AccessTokenHash,
    AccessToken, 
    AuthType, 
    AuthenticationFlow, 
    AuthorizationCode, 
    ClaimsVerificationError,
    CsrfToken, 
    OAuth2TokenResponse, 
    RequestTokenError, 
    Scope, 
    SignatureVerificationError,
    UserInfoError,
};

#[macro_use]
mod rp_common;

use rp_common::{
//    get_provider_metadata, 
//    http_client, 
    init_log, 
    issuer_url,  
//    register_client, 
    PanicIfFail,
};

fn handle_error<T: std::error::Error>(fail: &T, msg: &'static str) {
    let mut err_msg = format!("ERROR: {}", msg);
    let mut cur_fail: Option<&dyn std::error::Error> = Some(fail);
    while let Some(cause) = cur_fail {
        err_msg += &format!("\n    caused by: {}", cause);
        cur_fail = cause.source();
    }
    log::info!("{}", err_msg);
    exit(1);
}

#[test]
#[ignore]
pub fn web_sso_login() -> Result<(), anyhow::Error> {

    let my_username = "d45538";
    let my_password = "frederic";

    let realm = "edf-iot";
    let client_id = "5C-APP-001";

    let redirect_callback = "http://localhost:7070/callback";

    init_log(client_id);

    // init
    let _issuer_url = issuer_url("edf-iot");
    let provider_metadata = get_provider_metadata("edf-iot");
    //let registration_response = register_client(&provider_metadata, reg_request_fn);

    // FSI: https ?
    // Discover
    log::info!("issuer : {}", "http://idp-oidc-provider.test:8080/realms/".to_owned() + realm);
    let provider_metadata = CoreProviderMetadata::discover(
        &IssuerUrl::new(("http://idp-oidc-provider.test:8080/realms/".to_owned() + realm).to_string())?,
        http_client,
    )
    .unwrap_or_else(|err| {
        handle_error(&err, "Failed to discover OpenID Provider");
        //print!("Issuer URL : {}", IssuerUrl);
        unreachable!();
    });
    log::info!("discover ok");

    // Create an OpenID Connect client by specifying the client ID, client secret, authorization URL
    // and token URL.
    let client =
        CoreClient::from_provider_metadata(
            provider_metadata,
            ClientId::new(client_id.to_string()),
            Some(ClientSecret::new("XXXX".to_string())),
        )
        // Set the URL the user will be redirected to after the authorization process.
        // This example will be running its own server at localhost:8080.
        // See below for the server implementation.
        .set_redirect_uri(RedirectUrl::new(redirect_callback.to_string()).expect("Invalid redirect URL"))
        .enable_openid_scope();

    log::info!("client new ok");

    //----------------------------------------------------------------------
    // Authorization Endpoint Request (1/3) (GET | /auth/)
    //----------------------------------------------------------------------

     // Generate a PKCE challenge.
    let (pkce_challenge, pkce_verifier) = PkceCodeChallenge::new_random_sha256();
    
    log::info!("verifier : {:?}", pkce_verifier.secret());
    log::info!("challenge : {:?}", pkce_challenge);

    // Request Login page
    
    let acr_val = openidconnect::AuthenticationContextClass::new("sesameEDF".to_string());

        // Generate the full authorization URL.
    let (auth_url, csrf_state, nonce) = client
        .authorize_url(
            CoreAuthenticationFlow::AuthorizationCode,
            CsrfToken::new_random,
            Nonce::new_random,
        )
        // Set the desired scopes.
        .add_auth_context_value(acr_val)
        // Set the PKCE code challenge.
        .set_pkce_challenge(pkce_challenge)
        .url();

    log::info!("Request login page : {}", auth_url);
    //log::info!("Client : {:?}", client);

    //FSI: dirty cookie jar (wasm...)
    // Create a cookie jar we can share with the HTTP client
    let jar = Arc::new(Jar::default());

    let http_client = Client::builder()
        .redirect(Policy::none())
        .cookie_provider(Arc::clone(&jar))
        .cookie_store(true)
        .build()
        .unwrap();

    let response_form = http_client
        .execute(
            http_client.request(Method::GET, auth_url.as_str())
                .build()
                .unwrap(),
        )
        .unwrap();
    assert!(response_form.status().is_success()); // HTTP Status Code 200

    log::info!("request login page : {}", response_form.status());

    log::info!("Parse login page (response)");

    log::info!("Cookies ...");

    //log::info!("headers : {:?}", response_form.headers());
    
    //let headers_with_set_cookie = response_form.headers().get_all(http::header::SET_COOKIE);
    let mut headermap_with_set_cookie = HeaderMap::new(); //FSI:
    let headermap_with_set_cookie = response_form.headers().keys(); //FSI:

    let response_form_headermap = response_form.headers().clone(); //FSI:

    //let cookies = match response_form.headers().get::<SetCookie>() {
      //  Some(cookies) => cookies.join(","),
       // None => String::new(),
    //};

    //log::info!("Cookies : OK {:?}", headermap_with_set_cookie);
    //log::info!("Cookies : OK {:?}", response_form_headermap);

    //match response_form.headers().get(http::header::SET_COOKIE) {
        //Some(&SetCookie(ref content)) => log::info!("Cookie: {:?}", content),
        //_ => log::info!("No cookie found"),
    //}
    
    log::info!("Capture the login form ...");

    let html_form = response_form.text().unwrap();
    //log::info!("Dump html : \n{}", html_form);

    let regexp_rule = Regex::new(r##"<form\s+.*?\s+action="(.*?)"\s"##).unwrap();
    let formaction = regexp_rule.captures(html_form.as_str()).unwrap();
    let formaction_url = &formaction[1].to_string().replace("&amp;", "&");

    log::info!("Capture login form ok : {}", formaction_url);

    log::info!("Do the login (authenticate)...");

    let params = [("username", my_username.to_string()), ("password", my_password.to_string())];

    //let http_client = Client::builder()
      //  .redirect(Policy::none())
        //.build()
        //.unwrap();

    let response_login = http_client
        .execute(
            http_client.request(Method::POST, formaction_url)
                .form(&params)
                //.headers(response_form_headermap)
                .build()
                .unwrap(),
        )
        .unwrap();
    //assert!(response.status().is_success()); // HTTP Status Code 200
    log::info!("username/password provided via action form");
    log::info!("login page (authenticate) : {}", response_login.status());

    log::info!("Do the login (authenticate) : OK");

    log::info!("Extract authz code from redirect ...");

    //FSI: check redirect (seep python example)
    let raw_redirect_uri = response_login.headers().get(LOCATION).unwrap(); // "Location"
    let redirect_string = raw_redirect_uri.to_str().unwrap_or_default().to_string();
    log::info!("redirect uri : {}", redirect_string);

    let redirect_uri = reqwest::Url::parse(redirect_string.as_str());
    let params: HashMap<_, _> = redirect_uri.unwrap().query_pairs().into_owned().collect();

    let code = params.get("code");
    log::info!("code = {}", code.unwrap());
    log::info!("state = {}", params.get("state").unwrap()); //FSI: state consistency
    log::info!("session_state = {}", params.get("session_state").unwrap()); //FSI: session_state ?
    
    //redirect_uri = Url::parse(raw_redirect_uri.clone().);

    log::info!("Extract authz code from redirect (OK)");

    //FSI: ========================================================================================


        //// A very naive implementation of the redirect server.
        //let listener = TcpListener::bind("127.0.0.1:7070").unwrap();
//    
        //// Accept one connection
        //let (mut stream, _) = listener.accept().unwrap();
//    
        //let code;
        //let state;
        //{
            //let mut reader = BufReader::new(&stream);
//    
            //let mut request_line = String::new();
            //reader.read_line(&mut request_line).unwrap();
//    
            //let redirect_url = request_line.split_whitespace().nth(1).unwrap();
            //let url = Url::parse(&(redirect_callback.to_string() + redirect_url)).unwrap();
//    
            //let code_pair = url
                //.query_pairs()
                //.find(|pair| {
                    //let &(ref key, _) = pair;
                    //key == "code"
                //})
                //.unwrap();
//    
            //let (_, value) = code_pair;
            //code = AuthorizationCode::new(value.into_owned());
//    
            //let state_pair = url
                //.query_pairs()
                //.find(|pair| {
                    //let &(ref key, _) = pair;
                    //key == "state"
                //})
                //.unwrap();
//    
            //let (_, value) = state_pair;
            //state = CsrfToken::new(value.into_owned());
        //}

    // Once the user has been redirected to the redirect URL, you'll have access to the
    // authorization code. For security reasons, your code should verify that the `state`
    // parameter returned by the server matches `csrf_state`.

    //FSI: check state(s)
    log::info!("Have you checked state ? Client state : {}", csrf_state.secret());

    //----------------------------------------------------------------------
    // Token Endpoint Request (2/3) (POST | /access_token/)
    //----------------------------------------------------------------------

    log::info!("Exchange authz code for tokens ..");

    let exch_url = ("http://idp-oidc-provider.test:8080/realms/".to_owned() + realm + "/protocol/openid-connect/token").to_string();
    log::info!("Exchange url : {}", exch_url);

    let payload = [ ("grant_type","authorization_code"), 
                    ("client_id",client_id), 
                    ("redirect_uri",redirect_string.as_str()), 
                    ("code",code.unwrap()), 
                    ("code_verifier",pkce_verifier.secret()) ];

    let token_request = serde_json::json!({
        "grant_type":"authorization_code", 
        "client_id":client_id, 
        "redirect_uri":redirect_callback, 
        "code": code.unwrap().to_string(), 
        "code_verifier":pkce_verifier.secret()
    });
    log::info!("Payload : {}", token_request);

    let response_exchange = http_client
        .execute(
            http_client.request(Method::POST, exch_url.as_str())
                .header(reqwest::header::CONTENT_TYPE, HeaderValue::from_static("application/x-www-form-urlencoded"))
                //.header(reqwest::header::CONTENT_TYPE, HeaderValue::from_static("application/json"))
                .form(&token_request)
                .build()
                .unwrap(),
        )
        .unwrap();
    log::info!("Response : {:?}", response_exchange.status());
    log::info!("Response content : {:?}", response_exchange.text());

    //-- ID Token

    // Now you can exchange it for an access token and ID token.
    //let token_response =
        //client
            //.exchange_code(AuthorizationCode::new(code.unwrap().to_string()))
            //.set_pkce_verifier(pkce_verifier)
            //.request(http_client)
            //.panic_if_fail("failed to exchange authorization code for token")
            //;

    //log::info!("token_response : {:?}", token_response);

    // Extract the ID token claims after verifying its authenticity and nonce.
    //let id_token = token_response
    //.id_token()
    //.ok_or_else(|| anyhow!("Server did not return an ID token"))?;
    //let claims = id_token.claims(&client.id_token_verifier(), &nonce)?;

    // Finally, display the ID Token to verify we are using OIDC
    //log::info!("ID Token response: {:?}", id_token);
    //log::info!("ID Token response: {:?}", token_response.extra_fields().id_token());

    //-- Access Token

    // Verify the access token hash to ensure that the access token hasn't been substituted for
    // another user's.
    //if let Some(expected_access_token_hash) = claims.access_token_hash() {
        //let actual_access_token_hash = AccessTokenHash::from_token(
            //token_response.access_token(),
            //&id_token.signing_alg()?
        //)?;
        //if actual_access_token_hash != *expected_access_token_hash {
            //return Err(anyhow!("Invalid access token"));
        //}
    //}

    // The authenticated user's identity is now available. See the IdTokenClaims struct for a
    // complete listing of the available claims.
    //log::info!(
        //"User {} with e-mail address {} has authenticated successfully",
        //claims.subject().as_str(),
        //claims.email().map(|email| email.as_str()).unwrap_or("<not provided>"),
    //);

    //----------------------------------------------------------------------
    // UserInfo Endpoint Request (3/3) (POST | /userinfo/)
    //----------------------------------------------------------------------

    // If available, we can use the UserInfo endpoint to request additional information.

    // The user_info request uses the AccessToken returned in the token response. To parse custom
    // claims, use UserInfoClaims directly (with the desired type parameters) rather than using the
    // CoreUserInfoClaims type alias.
    //let userinfo: CoreUserInfoClaims = client
    //.user_info(token_response.access_token().to_owned(), None)
    //.map_err(|err| anyhow!("No user info endpoint: {:?}", err))?
    //.request(http_client)
    //.map_err(|err| anyhow!("Failed requesting user info: {:?}", err))?;

    // See the OAuth2TokenResponse trait for a listing of other available fields such as
    // access_token() and refresh_token().

    Ok(())
} 